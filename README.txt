zad.1
    Napisać program, w którym uruchamiane zadania pokazywane są na liście javax.swing.JList. Zadania z listy możemy odpytywac o ich stan, anulować, pokazywac ich wyniki, gdy są gotowe itp.
zad.2
    Napisać w sposób właściwy prosty przykładowy  serwer wielowątkowy TCP/IP (na gniazdach). Obsługę zleceń zrealizowac w postaci FutureTask. Zadbać o możliwość przerywania obsługi w każdym momencie.
zad.3
    Pokazać zastosowanie read/write locków i porównać ich efektywność w stosunku do zwykłej synchronizacji.
zad.4
    Napisać program Author-Writer z wykładu przy użyciu blokujących kolejek.
    Jako argumenty program otrzymuje napisy, które co sekundę ma generować Author.
    Writer ma je wypisywać na konsoli.

    Klasa Main ma następująca postać i nie można jej modyfikować:

            public class Main {
              public static void main(String[] args) {
                Author autor = new Author(args);
                new Thread(autor).start();
                new Thread(new Writer(autor)).start();
              }
            }
