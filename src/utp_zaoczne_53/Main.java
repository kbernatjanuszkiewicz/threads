/**
 *
 *  @author Bernat-Januszkiewicz Kacper S16508
 *
 */

package utp_zaoczne_53;


import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Main {
    private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    Lock writeLock = readWriteLock.writeLock();
    private static long number = 0;
    private static long syncNanoSec;
    private static long lockNanoSec;

    public static void main(String[] args) {
        Main main = new Main();
        long start = System.nanoTime();
        for(long i = 0; i < 100000; i++) {
            main.plainJob();
        }
        long finish = System.nanoTime();
        syncNanoSec = finish - start;
        System.out.println("Synchronized counting: " + getNumber());

        number = 0;
        start = System.nanoTime();
        for(long i = 0; i < 100000; i++) {
            main.lockJob();
        }
        finish = System.nanoTime();
        lockNanoSec = finish - start;

        System.out.println(main.getSyncTime());
        System.out.println(main.getLockedSyncTime());

        try {
            main.writeLock.lock();
            System.out.println("big ass number: " + getNumber());
        } finally {
            main.writeLock.unlock();
        }

    }

    public synchronized void plainJob() {
        number++;
    }

    public String getSyncTime() {
        return "Sync: " + syncNanoSec + "ns " + syncNanoSec / 1000000000.0 + "s";
    }

    public void lockJob() {
        writeLock.lock();
        number++;
        writeLock.unlock();
    }

    public static long getNumber() {
        return number;
    }

    public String getLockedSyncTime() {
        return "Lock: " + lockNanoSec + "ns " + lockNanoSec / 1000000000.0 + "s";
    }
}
