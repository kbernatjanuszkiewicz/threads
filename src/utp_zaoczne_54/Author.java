/**
 *
 *  @author Bernat-Januszkiewicz Kacper S16508
 *
 */

package utp_zaoczne_54;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Author implements Runnable {
    private LinkedList<String> wordsLibrary;
    private BlockingQueue<String> blockingQueue;
    private boolean poisonPill;

    public Author(String[] words) {
        this.wordsLibrary = new LinkedList<>(Arrays.asList(words));
        this.blockingQueue = new LinkedBlockingQueue<>();
        this.poisonPill = false;

    }

    public boolean isPoisoned() {
        return poisonPill;
    }


    public BlockingQueue<String> getQ() {
        return blockingQueue;
    }

    @Override
    public void run() {
        try {
            while(0 < wordsLibrary.size()) {
                Thread.sleep(1000);
                blockingQueue.add(wordsLibrary.remove());
            }
            poisonPill = true;
        }
        catch(InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}

