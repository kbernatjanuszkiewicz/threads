/**
 *
 *  @author Bernat-Januszkiewicz Kacper S16508
 *
 */

package utp_zaoczne_54;
import java.util.concurrent.BlockingQueue;

public class Writer implements Runnable {
    Author author;

    public Writer(Author author) {
        this.author = author;
    }

    @Override
    public void run() {
        try {
            BlockingQueue<String> blockingQueue = author.getQ();
            while(!author.isPoisoned()) {
                System.out.println(blockingQueue.take());
            }
        }
        catch(InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
